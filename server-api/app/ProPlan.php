<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProPlan extends Model
{
	/**
	 * The database table tb_pro_plan by the model.
	 *
	 * @var string
	 */
	protected $table = 'tb_pro_plan';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'value', 'one_month_promotion',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
    ];
}
