<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{

	/**
	 * The database table tb_subscription by the model.
	 *
	 * @var string
	 */
	protected $table = 'tb_subscription';
	
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'active',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_pro_id', 'pro_plan_id', 
    ];
    
    public function userPro(){
    	return $this->belongsTo('App\UserPro');
    }
    
    public function proPlan(){
    	return $this->belongsTo('App\ProPlan');
    }
}
