<?php

namespace App\Http\Controllers;



use App\VehicleModel;

class VehicleModelController extends Controller
{
    //
	public function __construct(){
		//$this->middleware('auth.basic', ['only' => 'store']);
		//$this->middleware('auth.basic');
		$this->middleware('jwt.auth');
	}
	
	public function index(){
		$vehicleModels = VehicleModel::all(); //Not a good idea
		return response()->json([
				'message' => $vehicleModels
		], 200);
	}

// 	public function show($id){
// 		$vehicleModel = VehicleModel::find($id);
	
// 		if(!$vehicleModel){
// 			return response()->json([
// 					'error' => [
// 							'message' => 'Vehicle does not exist'
// 					]
// 			], 404);
// 		}
	
// 		return response()->json([
// 				'data' => $vehicleModel
// 		], 200);
// 	}

	private function transformCollection($users){
		return array_map([$this, 'transform'], $users->toArray());
	}
	
	private function transform($user){
		return [
				//'joke_id' => $user['id'],
				//'joke' => $user['body']
		];
	}
}
