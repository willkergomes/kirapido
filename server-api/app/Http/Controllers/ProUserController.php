<?php

namespace App\Http\Controllers;



use App\ProUser;

class ProUserController extends Controller
{
    //
	
	public function __construct(){
		//$this->middleware('auth.basic', ['only' => 'store']);
		//$this->middleware('auth.basic');
		$this->middleware('jwt.auth');
	}
	
	public function index(){
		$proUsers = ProUser::all(); //Not a good idea
		return response()->json([
				'message' => $proUsers
		], 200);
	}

	public function show($id){
		$proUser = ProUser::find($id);
	
		if(!$proUser){
			return response()->json([
					'error' => [
							'message' => 'UserPro does not exist'
					]
			], 404);
		}
	
		return response()->json([
				'data' => $proUser
		], 200);
	}

	private function transformCollection($users){
		return array_map([$this, 'transform'], $users->toArray());
	}
	
	private function transform($user){
		return [
				//'joke_id' => $user['id'],
				//'joke' => $user['body']
		];
	}
}
