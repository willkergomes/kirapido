<?php

namespace App\Http\Controllers;



use App\ProPlan;

class ProPlanController extends Controller
{
    //
	
	public function __construct(){
		$this->middleware('jwt.auth');
	}
	
	public function index(){
		$proPlans = ProPlan::all(); //Not a good idea
		return response()->json([
				'data' => $proPlans
		], 200);
	}

	public function show($id){
		$proPlan = ProPlan::find($id);
	
		if(!$proPlan){
			return response()->json([
					'error' => [
							'message' => 'ProPlan does not exist'
					]
			], 404);
		}
	
		return response()->json([
				'data' => $proPlan
		], 200);
	}
}
