<?php

namespace App\Http\Controllers;



use App\PaymentMethod;

class PaymentMethodController extends Controller
{
    //
	
	public function __construct(){
		$this->middleware('jwt.auth');
	}
	
	public function index(){
		$payMets = PaymentMethod::all(); //Not a good idea
		return response()->json([
				'data' => $payMets
		], 200);
	}

	public function show($id){
		$payMet = PaymentMethod::find($id);
	
		if(!$payMet){
			return response()->json([
					'error' => [
							'message' => 'PaymentMethod does not exist'
					]
			], 404);
		}
	
		return response()->json([
				'data' => $payMet
		], 200);
	}
}
