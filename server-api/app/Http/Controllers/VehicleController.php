<?php

namespace App\Http\Controllers;



use App\Vehicle;
use JWTAuth;
use App\ProUser;

class VehicleController extends Controller
{
    //
	
	public function __construct(){
		//$this->middleware('auth.basic', ['only' => 'store']);
		//$this->middleware('auth.basic');
		$this->middleware('jwt.auth');
	}
	
// 	public function index(){
// 		$vehicles = Vehicle::all(); //Not a good idea
// 		return response()->json([
// 				'message' => $vehicles
// 		], 200);
// 	}
	
	public function index()
	{
		//$search_term = $request->input('search');
		//$limit = $request->input('limit')?$request->input('limit'):5;
		
// 		if ($search_term)
// 		{
			$user = JWTAuth::parseToken()->authenticate();
			$proUser = ProUser::find($user->id);
			
			$vehicles = Vehicle::orderBy('id', 'DESC')
				->where('pro_user_id', '=', "$proUser->id")->with(
				array('proUser'=>function($query){
					$query->select('id');
				})
				)->with(
				array('vehicleModel'=>function($query){
					$query->select('id', 'description');
				})
				)->select('id', 'pro_user_id', 'vehicle_model_id', 
						'license_plate', 'manufacturing_company', 'manufacture_year',
						'photo', 'validated', 'active')
				 ->paginate(5);
	
// 				$vehicles->appends(array(
// 						'search' => $search_term
// 				));
			return response()->json($this->transformCollection($vehicles), 200);
			//return response()->json($vehicles, 200);
// 		}
// 		else
// 		{
// 			$vehicles = Vehicle::with(
// 				array('proUser'=>function($query){
// 					$query->select('id');
// 				})
// 				)->select('id', 'pro_user_id', 'vehicle_model_id', 
// 						'license_plate', 'manufacturing_company', 'manufacture_year',
// 						'photo', 'validated', 'active')
// 				 ->paginate(5);
// 			return response()->json($this->transformCollection($vehicles), 200);
// 			//return response()->json($vehicles, 200);
		
// 		}
	}

	public function show($id){
		
		$vehicle = Vehicle::with(
			array('proUser'=>function($query){
				$query->select('id');
			})
			)->with(
			array('vehicleModel'=>function($query){
				$query->select('id', 'description');
			})
			)->find($id);
	
		if(!$vehicle){
			return response()->json([
					'error' => [
							'message' => 'Vehicle does not exist'
					]
			], 404);
		}
	
		return response()->json([
				'data' => $vehicle
		], 200);
	}

	public function store(Request $request)
	{

		if(! $request->vehicle_model_id or ! $request->id or !$vehicle->photo){
			return response()->json([
					'error' => [
							'message' => 'Please Provide photo, vehicle_model_id and id'
					]
			], 422);
		}
		$user = JWTAuth::parseToken()->authenticate();
		$proUser = ProUser::find($user->id);
		
// 		$vehicle = Vehicle::create($request->all());
		$vehicle = Vehicle::create([
				'pro_user_id' => $proUser->id,
				'vehicle_model_id' => $request->vehicle_model_id,
				'license_plate' => $request->postcode,
				'photo' => $request->photo,
				'manufacture_year' => $request->manufacture_year,
				'validated' => false,
				'active' => true,
				'created_at' => new DateTime('now')
		]);
	
		return response()->json([
				'message' => 'Vehicle Created Succesfully',
				//'data' => $this->transform($vehicle)
				'data' => $vehicle
		]);
	}
	
	public function update(Request $request, $id)
	{
		if(! $request->vehicle_model_id or ! $request->id or !$vehicle->photo){
			return response()->json([
					'error' => [
							'message' => 'Please Provide photo, vehicle_model_id and id'
					]
			], 422);
		}
		$user = JWTAuth::parseToken()->authenticate();
		$proUser = ProUser::find($user->id);
	
		$vehicle = Vehicle::find($id);
		if($proUser->id <> $vehicle->pro_user_id){

			return response()->json([
					'error' => [
							'message' => 'Not valid pro_user_id'
					]
			], 422);
		}
		
		$vehicle->vehicle_model_id = $request->vehicle_model_id;
		$vehicle->license_plate = $request->license_plate;
		$vehicle->manufacture_year = $request->manufacture_year;
		$vehicle->photo = $request->photo;
		$vehicle->validated = false;
		$vehicle->active = true;
		$vehicle->updated_at = new DateTime('now');
		$vehicle->save();
	
		return response()->json([
				'message' => 'Vehicle Updated Succesfully'
		]);
	}
	
	public function destroy($id)
	{
		Vehicle::destroy($id);
	}

// 	private function transformCollection($vehicles){
// 		return array_map([$this, 'transform'], $vehicles->toArray());
		
// 	}

	private function transformCollection($vehicles){
		$vehiclesArray = $vehicles->toArray();
		return [
				'total' => $vehiclesArray['total'],
				'per_page' => intval($vehiclesArray['per_page']),
				'current_page' => $vehiclesArray['current_page'],
				'last_page' => $vehiclesArray['last_page'],
				'next_page_url' => $vehiclesArray['next_page_url'],
				'prev_page_url' => $vehiclesArray['prev_page_url'],
				'from' => $vehiclesArray['from'],
				'to' =>$vehiclesArray['to'],
				//'data' => array_map([$this, 'transform'], $vehiclesArray['data'])
				'data' => $vehiclesArray['data']
		];
	}
	
	private function transform($vehicle){
		return [
				//'vehicle_id' => $vehicle['id'],
				//'vehicle' => $vehicle['body']
		];
	}
}
