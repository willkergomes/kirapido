<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleModel extends Model
{

	/**
	 * The database table tb_vehicle_model by the model.
	 *
	 * @var string
	 */
	protected $table = 'tb_vehicle_model';
	
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description', 'manufacturing_company', 'year', 'active',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    		
    ];
	
	public function vehicles(){
		return $this->hasMany('App\Vehicle');
	}
}
