<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{

	/**
	 * The database table tb_vehicle by the model.
	 *
	 * @var string
	 */
	protected $table = 'tb_vehicle';
	
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'license_plate', 'manufacture_year', 'photo', 'active',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'pro_plan_id', 
    ];
    
    public function proUser(){
    	return $this->belongsTo('App\ProPlan');
    }
    
    public function vehicleModel(){
    	return $this->belongsTo('App\VehicleModel');
    }
}
