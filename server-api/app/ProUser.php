<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProUser extends Model
{
    //
	/**
	 * The database table tb_pro_user by the model.
	 *
	 * @var string
	 */
	protected $table = 'tb_pro_user';
	
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
			'updated_at',
	];
	
	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
	];
	
	public function user(){
		return $this->belongsTo('App\User');
	}
	
	public function subscriptions(){
		return $this->hasMany('App\Subscription');
	}
}
