<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbVehicleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('tb_vehicle', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pro_user_id')->unsigned();
            $table->foreign('pro_user_id')->references('id')->on('tb_pro_user');
            $table->integer('vehicle_model_id')->unsigned();
            $table->foreign('vehicle_model_id')->references('id')->on('tb_vehicle_model');
            $table->string('license_plate');
            $table->integer('manufacture_year');
            $table->binary('photo');
            $table->boolean('validated');
            $table->boolean('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('tb_vehicle');
    }
}
