<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbSubscriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('tb_subscription', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pro_user_id')->unsigned();
            $table->foreign('pro_user_id')->references('id')->on('tb_pro_user');
            $table->integer('pro_plan_id')->unsigned();
            $table->foreign('pro_plan_id')->references('id')->on('tb_pro_plan');
            $table->integer('payment_method_id')->unsigned();
            $table->foreign('payment_method_id')->references('id')->on('tb_payment_method');
            $table->boolean('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('tb_subscription');
    }
}
