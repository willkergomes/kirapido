<?php 

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // DOMINIO
        $this->call(ProPlanTableSeeder::class);
        $this->call(VehicleModelTableSeeder::class);
        $this->call(PaymentMethodTableSeeder::class);

        // ISOLADAS
        $this->call(UsersTableSeeder::class);
        
        // DEPENDENTES
        $this->call(ProUserTableSeeder::class);
        $this->call(VehicleTableSeeder::class);
        $this->call(SubscriptionTableSeeder::class);
 
        Model::reguard();
    }
}
