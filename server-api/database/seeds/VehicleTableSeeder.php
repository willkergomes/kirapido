<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use App\Vehicle;
use Laracasts\TestDummy\Factory as TestDummy;

class VehicleTableSeeder extends Seeder
{
    public function run()
    {
        // TestDummy::times(20)->create('App\Post');
//             $table->integer('pro_user_id')->unsigned();
//             $table->foreign('pro_user_id')->references('id')->on('tb_pro_user');
//             $table->integer('vehicle_model_id')->unsigned();
//             $table->foreign('vehicle_model_id')->references('id')->on('tb_vehicle_model');
//             $table->string('license_plate');
//             $table->integer('manufacture_year');
//             $table->binary('photo');
//             $table->boolean('active');
    	$faker = Faker\Factory::create();
    	
    	foreach(range(1,21) as $index)
    	{
    		Vehicle::create([
    				'pro_user_id' => $faker->numberBetween($min = 1, $max = 5),
    				'vehicle_model_id' => $faker->numberBetween($min = 1, $max = 5),
    				'license_plate' => $faker->postcode,
    				'manufacture_year' => $faker->year,
    				'validated' => $faker->boolean(2),
    				'active' => $faker->boolean(2),
    				'created_at' => new DateTime('now')
    		]);
    	}
    }
}
