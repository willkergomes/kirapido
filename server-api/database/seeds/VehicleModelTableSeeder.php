<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use App\VehicleModel;
use Laracasts\TestDummy\Factory as TestDummy;

class VehicleModelTableSeeder extends Seeder
{
    public function run()
    {
        // TestDummy::times(20)->create('App\Post');
    	$faker = Faker\Factory::create();
    	
    	foreach(range(1,5) as $index)
    	{
    		VehicleModel::create([
    				'description' => $faker->paragraph($nbSentences = 3),
    				'manufacturing_company' => $faker->paragraph($nbSentences = 1),
    				'year' => $faker->year,
    				'active' => $faker->boolean(2),
    				'created_at' => new DateTime('now')
    		]);
    	}
    }
}
