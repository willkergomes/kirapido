<?php 

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use App\ProUser;
use Laracasts\TestDummy\Factory as TestDummy;

class ProUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // TestDummy::times(20)->create('App\Post');
    	$faker = Faker\Factory::create();
    	
    	foreach(range(1,5) as $index)
    	{
    		ProUser::create([
    				'user_id' =>$faker->numberBetween($min = 1, $max = 5),
    				'created_at' => new DateTime('now')
    		]);
    	}
    }
}
