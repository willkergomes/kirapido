<?php 

use Illuminate\Database\Seeder;
use App\User;
 
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//         $faker = Faker\Factory::create(); 
		$faker = new Faker\Generator();
		$faker->addProvider(new Faker\Provider\pt_BR\Person($faker));
		$faker->addProvider(new Faker\Provider\Internet($faker));
		$faker->addProvider(new Faker\Provider\DateTime($faker));
		
// 		$faker->addProvider(new Faker\Provider\pt_BR\Address($faker));
// 		$faker->addProvider(new Faker\Provider\pt_BR\PhoneNumber($faker));
// 		$faker->addProvider(new Faker\Provider\pt_BR\Company($faker));
// 		$faker->addProvider(new Faker\Provider\Lorem($faker));
         
        foreach(range(1,5) as $index)
        {
        	$faker->seed($index);
        	
            User::create([
                'name' => $faker->name,
                'email' => $faker->email,
            	'cpf' => $faker->cpf,
            	'rg' => $faker->rg,
            	'birthday' => $faker->dateTimeThisCentury,
                'password' =>bcrypt('123456')
            	//'password' => Hash::make('123456')
            ]);
        }
    }
}
