<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use App\PaymentMethod;
use Laracasts\TestDummy\Factory as TestDummy;

class PaymentMethodTableSeeder extends Seeder
{
    public function run()
    {
        // TestDummy::times(20)->create('App\Post');
    	$faker = Faker\Factory::create();

    	PaymentMethod::create([
    			'name' => 'Dinheiro',
//     			'description' => '',
    			'active' => true,
    			'created_at' => new DateTime('now')
    	]);

    	PaymentMethod::create([
    			'name' => 'Fatura',
//     			'description' => '',
    			'active' => true,
    			'created_at' => new DateTime('now')
    	]);

    	PaymentMethod::create([
    			'name' => 'Cart�o de D�bito',
//     			'description' => '',
    			'active' => true,
    			'created_at' => new DateTime('now')
    	]);

    	PaymentMethod::create([
    			'name' => 'Cart�o de Cr�dito',
//     			'description' => '',
    			'active' => true,
    			'created_at' => new DateTime('now')
    	]);
    }
}
