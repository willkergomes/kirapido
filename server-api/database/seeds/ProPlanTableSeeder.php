<?php 

use Illuminate\Database\Seeder;
use App\ProPlan;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class ProPlanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // TestDummy::times(20)->create('App\Post');
    	$faker = Faker\Factory::create();

    	ProPlan::create([
    			'name' => 'Motoboy',
    			'value' => 19.90,
    			'description' => $faker->paragraph($nbSentences = 3),
    			'one_month_promotion' => true,
    			'created_at' => new DateTime('now')
    	]);

    	ProPlan::create([
    			'name' => 'Motofretista',
    			'value' => 29.90,
    			'one_month_promotion' => true,
    			'created_at' => new DateTime('now')
    	]);

    	ProPlan::create([
    			'name' => 'Motoboy e Motofretista',
    			'value' => 49.90,
    			'one_month_promotion' => true,
    			'created_at' => new DateTime('now')
    	]);
    }
}
