<?php 

use Illuminate\Database\Seeder;
use App\Subscription;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class SubscriptionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // TestDummy::times(20)->create('App\Post');
    	$faker = Faker\Factory::create();
    	
    	foreach(range(1,5) as $index)
    	{
    		Subscription::create([
    				'pro_user_id' => $faker->numberBetween($min = 1, $max = 5),
    				'pro_plan_id' => $faker->numberBetween($min = 1, $max = 3),
    				'payment_method_id' => $faker->numberBetween($min = 1, $max = 4),
    				'active' => $faker->boolean(2),
    				'created_at' => new DateTime('now')
    		]);
    	}
    }
}
