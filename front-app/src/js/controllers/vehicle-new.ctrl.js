"use strict";

starter.controllers.controller('VehicleNewCtrl',
		['$scope', '$stateParams', '$ionicPopup', '$state', '$ionicLoading', 'VehicleModelService', 'VehicleService',
		function($scope, $stateParams, $ionicPopup, $state, $ionicLoading, VehicleModelService, VehicleService) {

	var vm = this;
	$scope.vm = vm;

	vm.vehicle = {};
	vm.vehicleModels = [];

	vm.init = function () {
		$ionicLoading.show();
		// Carrega combo de VehicleModel
		VehicleModelService.findAll().success(function(response) {
			console.log(response.message);
			console.log(response.data);
			vm.vehicleModels = response.data;
      $ionicLoading.hide();
		}).error(function(response, status) {
			console.log(response.message);
		 	console.log("error");
			alert('data error!');
			vm.vehicleModels = [];
      $ionicLoading.hide();
		});
  };

	vm.addVehicle = function () {
		VehicleService.addVehicle(vm.vehicle).success(function(response) {
			console.log(response.message);
		}).error(function(response, status) {
			console.log(response.message);
			console.log("error");
			alert('data error!');
		});

	};

	vm.updateVehicle = function () {
		VehicleService.updateVehicle(vm.vehicle).success(function(response) {
			console.log(response.message);
		}).error(function(response, status) {
			console.log(response.message);
			console.log("error");
			alert('data error!');
		});
	};

	vm.deleteVehicle = function () {
		VehicleService.deleteVehicle(vm.vehicle.id).success(function(response) {
			console.log(response.message);
		}).error(function(response, status) {
			console.log(response.message);
			console.log("error");
			alert('data error!');
		});
	};

	vm.init();

}]);
