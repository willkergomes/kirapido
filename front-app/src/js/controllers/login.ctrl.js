"use strict";

starter.controllers.controller('LoginCtrl', ['$scope', '$state', '$q', 'AuthService', '$rootScope', '$ionicLoading', '$ionicModal', 'UserService', '$auth',
                                      function($scope, $state, $q, AuthService, $rootScope, $ionicLoading, $ionicModal, UserService, $auth) {

  var vm = this;

  vm.user = {};
  vm.userLost = {};

  //$scope.vm = vm;

	vm.cancelar = function() {
    $state.go('welcome');
	}

  vm.login = function() {
		$ionicLoading.show();
    AuthService.login(vm.user.email, vm.user.password, function(response) {
      if(!response.loginError) {
        $rootScope.currentUser = response.data;
        vm.user = {};
        $state.go('tab.home');
    		$ionicLoading.hide();
      } else {
        $ionicLoading.hide();
        alert(response.loginErrorText);
        vm.user.password = '';
        console.log(response.loginErrorText);
      }
    });

  };

  var isAndroid = function(platform) {
    return ionic.Platform.isAndroid();
  }

  $ionicModal.fromTemplateUrl('templates/forgot-password.modal.html', function($ionicModal) {
      vm.forgotPasswordModal = $ionicModal;
    }, {
      // Use our scope for the scope of the modal to keep it simple
      scope: $scope,
      // The animation we want to use for the modal entrance
      animation: 'slide-in-up'
  });

  vm.abrirForgotPasswordModal = function() {
    vm.forgotPasswordModal.show();
    vm.userLost = {};
    vm.userLost.email = vm.user.email;
  }

  vm.fecharResetPasswordModal = function() {
    vm.forgotPasswordModal.hide();
    vm.userLost = {};
  }

  vm.resetPassword = function(user) {
    UserService.resetPassword(user);
  }

  // This is the success callback from the login method
  var fbLoginSuccess = function(response) {
    if (!response.authResponse){
      fbLoginError("Cannot find the authResponse");
      return;
    }

    var authResponse = response.authResponse;

    getFacebookProfileInfo(authResponse)
    .then(function(profileInfo) {
      // For the purpose of this example I will store user data on local storage
      UserService.setUser({
        authResponse: authResponse,
				userID: profileInfo.id,
				name: profileInfo.name,
				email: profileInfo.email,
        picture : "http://graph.facebook.com/" + authResponse.userID + "/picture?type=large"
      });
      $ionicLoading.hide();
      $state.go('tab.home');
    }, function(fail){
      // Fail get profile info
      console.log('profile info fail', fail);
    });
  };

  // This is the fail callback from the login method
  var fbLoginError = function(error){
    console.log('fbLoginError', error);
    $ionicLoading.hide();
  };

  // This method is to get the user profile info from the facebook api
  var getFacebookProfileInfo = function (authResponse) {
    var info = $q.defer();

    facebookConnectPlugin.api('/me?fields=email,name&access_token=' + authResponse.accessToken, null,
      function (response) {
				console.log(response);
        info.resolve(response);
      },
      function (response) {
				console.log(response);
        info.reject(response);
      }
    );
    return info.promise;
  };

  //This method is executed when the user press the "Login with facebook" button
  vm.facebookSignIn = function() {

    if(isAndroid()) {
      facebookConnectPlugin.getLoginStatus(function(success){
        if(success.status === 'connected'){
          // The user is logged in and has authenticated your app, and response.authResponse supplies
          // the user's ID, a valid access token, a signed request, and the time the access token
          // and signed request each expire
          console.log('getLoginStatus', success.status);

      		// Check if we have our user saved
      		var user = UserService.getUser();

      		if(!user.userID){
  					getFacebookProfileInfo(success.authResponse)
  					.then(function(profileInfo) {
  						// For the purpose of this example I will store user data on local storage
  						UserService.setUser({
  							authResponse: success.authResponse,
  							userID: profileInfo.id,
  							name: profileInfo.name,
  							email: profileInfo.email,
  							picture : "http://graph.facebook.com/" + success.authResponse.userID + "/picture?type=large"
  						});

              $state.go('login');
  					}, function(fail){
  						// Fail get profile info
  						console.log('profile info fail', fail);
  					});
  				}else{
              $state.go('tab.home');
  				}
        } else {
          // If (success.status === 'not_authorized') the user is logged in to Facebook,
  				// but has not authenticated your app
          // Else the person is not logged into Facebook,
  				// so we're not sure if they are logged into this app or not.

  				console.log('getLoginStatus', success.status);

  				$ionicLoading.show({
            template: 'Logging in...'
          });

  				// Ask the permissions you need. You can learn more about
  				// FB permissions here: https://developers.facebook.com/docs/facebook-login/permissions/v2.4
          facebookConnectPlugin.login(['email', 'public_profile'], fbLoginSuccess, fbLoginError);
        }
      });
    } else {
      $auth.authenticate('facebook');
    }
  };
}]);
