"use strict";

starter.controllers.controller('VehicleEditCtrl',
		['$scope', '$stateParams', '$ionicPopup', '$state', '$ionicLoading', 'VehicleModelService', 'VehicleService',
		function($scope, $stateParams, $ionicPopup, $state, $ionicLoading, VehicleModelService, VehicleService) {

	var vm = this;
	$scope.vm = vm;

	vm.vehicle = {};
	vm.vehicleModels = [];

	vm.init = function () {
		$ionicLoading.show();
		// Carrega combo de VehicleModel
		VehicleModelService.findAll().success(function(response) {
			console.log(response.message);
			console.log(response.data);
			vm.vehicleModels = response.data;
      $ionicLoading.hide();
		}).error(function(response, status) {
			console.log(response.message);
		 	console.log("error");
			alert('data error!');
			vm.vehicleModels = [];
      $ionicLoading.hide();
		});

		var vehicleId = $stateParams.id;

		$ionicLoading.show();
    // Verifica se é uma busca para edição
    if($stateParams != null && vehicleId != null) {
			VehicleService.findById(vehicleId).success(function(response) {
				console.log(response.message);
				console.log(response.data);
        if(response.data) {
	 				vm.vehicle = response.data;
        } else {
					vm.vehicle =  {};
        }
	      $ionicLoading.hide();
			}).error(function(response, status) {
				console.log(response.message);
			 	console.log("error");
				alert('data error!');
	      $ionicLoading.hide();
			});
    }
  };

	vm.addVehicle = function () {
		VehicleModelService.addVehicle(vm.vehicle).success(function(response) {
			console.log(response.message);
		}).error(function(response, status) {
			console.log(response.message);
			console.log("error");
			alert('data error!');
		});

	};

	vm.updateVehicle = function () {
		VehicleModelService.updateVehicle(vm.vehicle).success(function(response) {
			console.log(response.message);
		}).error(function(response, status) {
			console.log(response.message);
			console.log("error");
			alert('data error!');
		});
	};

	vm.deleteVehicle = function() {
		$ionicPopup.show({
			title: 'Confirma a exclusão do veículo de placa, '+vm.vehicle.license_plate+'?',
			buttons: [{ // Array[Object] (optional). Buttons to place in the popup footer.
		    text: 'Não',
		    type: 'button-default',
		    onTap: function(e) {
		    }
		  }, {
		    text: 'Sim',
		    type: 'button-positive',
		    onTap: function(e) {
					$ionicLoading.show({
					  template: 'Excluindo...'
					});
					VehicleService.deleteVehicle(vm.vehicle.id).success(function(response) {
						console.log(response);
						$state.go('tab.vehicle-list');
			      $ionicLoading.hide();
					}).error(function(response, status) {
						console.log("response: " + response);
					 	console.log("status: "+ status);
						vm.config.error = true;
			      $ionicLoading.hide();
					});
		    }
		  }]
		});
	};

	vm.init();

}]);
