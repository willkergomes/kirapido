"use strict";

starter.controllers.controller('HomeCtrl', ['$scope', 'UserService', '$ionicPopup', '$state', '$ionicLoading', '$auth', '$rootScope',
		function($scope, UserService, $ionicPopup, $state, $ionicLoading, $auth, $rootScope) {
	var vm = this;
	vm.user = UserService.getUser();

  var isAndroid = function(platform) {
    return ionic.Platform.isAndroid();
  }

	vm.showLogOutMenu = function() {
		$ionicPopup.show({
			title: 'Deseja sair do sistema?',
			buttons: [{ // Array[Object] (optional). Buttons to place in the popup footer.
		    text: 'Cancelar',
		    type: 'button-default',
		    onTap: function(e) {
		    }
		  }, {
		    text: 'Sair',
		    type: 'button-positive',
		    onTap: function(e) {
					$ionicLoading.show({
					  template: 'Logging out...'
					});

					// logout from local
	        $auth.logout().then(function() {
	          localStorage.removeItem('user');
	          $rootScope.currentUser = null;
	          $state.go('welcome');
	        });

					if(isAndroid()) {
		        // Facebook logout
		        facebookConnectPlugin.logout(function() {
		          $ionicLoading.hide();
		          $state.go('welcome');
		        },
		        function(fail) {
							alert(fail);
		          $ionicLoading.hide();
		  				ionic.Platform.exitApp(); // stops the app
		        });
					} else {
						$ionicLoading.hide();
					}
		    }
		  }]
		});
	};
}]);
