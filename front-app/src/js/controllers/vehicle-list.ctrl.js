

starter.controllers.controller('VehicleListCtrl',
		['$scope', 'VehicleService', '$ionicPopup', '$state', '$ionicLoading', '$timeout', '$ionicPopup',
		function($scope, VehicleService, $ionicPopup, $state, $ionicLoading, $timeout, $ionicPopup) {

	var vm = this;
	//$scope.vm = vm;
	vm.vehicles = [];
	vm.vehicle = {};
	vm.config = {
        showDelete: false,
        title: 'Meus Veículos',
				error: false,
				nextPage: 0,
				currentPage: 1,
				lastPage: 0
    };

	vm.init = function () {
		$ionicLoading.show();
		VehicleService.findAll().success(function(response) {
			paginationConfig(response);
			console.log(response.message);
			console.log(response.data);
			vm.vehicles = response.data;
      $ionicLoading.hide();
		}).error(function(response, status) {
			console.log("response: " + response);
		 	console.log("status: "+ status);
			vm.vehicles = [];
			vm.config.error = true;
      $ionicLoading.hide();

			if(status == 400) {
	      $scope.$broadcast('server-status-400');
			}

		});
	};

	function paginationConfig(response) {
			vm.config.currentPage = response.current_page;
			vm.config.lastPage = response.last_page;
			if(vm.moreDataCanBeLoaded()) {
				vm.config.nextPage = response.next_page_url;
			} else {
				vm.config.nextPage = null;
			}
	}

	vm.showHideDelete = function() {
		return vm.config.showDelete = !vm.config.showDelete;
	}

	vm.addVehicle = function() {
		vm.vehicle = {};
		$state.go('tab.vehicle-new');
	};

	vm.editVehicle = function(vehicle) {
		vm.vehicle = vehicle;
		$state.go('tab.vehicle-detail', {id: vehicle.id});
	};

	vm.deleteVehicle = function(vehicle) {

		$ionicPopup.show({
			title: 'Confirma a exclusão do veículo de placa, '+vehicle.license_plate+'?',
			buttons: [{ // Array[Object] (optional). Buttons to place in the popup footer.
		    text: 'Não',
		    type: 'button-default',
		    onTap: function(e) {
		    }
		  }, {
		    text: 'Sim',
		    type: 'button-positive',
		    onTap: function(e) {
					$ionicLoading.show({
					  template: 'Excluindo...'
					});
					VehicleService.deleteVehicle(vehicle.id).success(function(response) {
			    	vm.vehicles.splice(vm.vehicles.indexOf(vehicle), 1);
						console.log(response);
			      $ionicLoading.hide();
					}).error(function(response, status) {
						console.log("response: " + response);
					 	console.log("status: "+ status);
						vm.vehicles = [];
						vm.config.error = true;
			      $ionicLoading.hide();
					});
		    }
		  }]
		});
	};

  vm.doRefresh = function() {
			vm.init();
      $scope.$broadcast('scroll.refreshComplete');
  };

  vm.loadMore = function() {
      VehicleService.findAll(vm.config.nextPage).success(function(response) {
				paginationConfig(response);
        vm.vehicles = vm.vehicles.concat(response.data);
        $scope.$broadcast('scroll.infiniteScrollComplete');
			}).error(function(response, status) {
				console.log("response: " + response);
			 	console.log("status: "+ status);
				vm.config.error = true;
        $scope.$broadcast('scroll.infiniteScrollComplete');
			});
  };

	vm.moreDataCanBeLoaded = function() {
		return vm.config.currentPage < vm.config.lastPage;
	}

  $scope.$on('$stateChangeSuccess', function() {
    vm.loadMore();
  });

  $scope.$on('server-status-400', function (viewInfo, state) {
		$timeout(function() {
			$state.go('welcome');
		}, 5000);
  });

  $scope.$on('$ionicView.unloaded', function (viewInfo, state) {
      console.log('CTRL - $ionicView.unloaded', viewInfo, state);
  });

  $scope.$on('$ionicView.unloaded', function (viewInfo, state) {
      console.log('CTRL - $ionicView.unloaded', viewInfo, state);
  });

	vm.init();
}]);
