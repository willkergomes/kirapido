"use strict";

starter.controllers.controller('RegisterUserCtrl', ['$scope', 'UserService', '$state', '$ionicLoading',
		function($scope, UserService, $state, $ionicLoading){

	var vm = this;

	vm.user = JSON.parse(localStorage.getItem('user'));
	if(!vm.user) {
		$state.go('register');
	}

	vm.avancar = function() {
		if(vm.user.pro) {
			localStorage.setItem('user', vm.user);
			$state.go('registeruserpro');
		} else {
			$state.go('tab.home');
		}
	}


}]);
