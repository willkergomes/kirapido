// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
var starter = {
  controllers: angular.module('starter.controllers', []),
  services: angular.module('starter.services', [])
};

starter.app = angular.module('starter',
  ['ionic',
  'starter.controllers',
  'starter.services',
  'ngCordova',
  'satellizer',
  'permission',
  'angularMoment'])

.run(['$ionicPlatform', '$rootScope', 'PermissionStore', 'RoleStore',
    function($ionicPlatform, $rootScope, PermissionStore, RoleStore) {
  $rootScope.VERSION = window.VERSION;
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
    // hide the splash screen only after everything's ready (avoid flicker)
    // requires keyboard plugin and confix.xml entry telling the splash screen to stay until explicitly told
    if(navigator.splashscreen){
      navigator.splashscreen.hide();
    }

    RoleStore.defineRole('isloggedin', [], function (stateParams) {
      // If the returned value is *truthy* then the user has the role, otherwise they don't
      // console.log("isloggedin ", $auth.isAuthenticated());
      if ($auth.isAuthenticated()) {
        return true; // Is loggedin
      }
      return false;
    });
    RoleStore.defineRole('anonymous', [], function (stateParams) {
      // If the returned value is *truthy* then the user has the role, otherwise they don't
      // var User = JSON.parse(localStorage.getItem('user'));
      // console.log("anonymous ", $auth.isAuthenticated());
      if (!$auth.isAuthenticated()) {
        return true; // Is anonymous
      }
      return false;
    });

  });
}])

.config(['$stateProvider', '$urlRouterProvider', '$authProvider', '$compileProvider', '$locationProvider',
    function($stateProvider, $urlRouterProvider, $authProvider, $compileProvider, $locationProvider) {

  $compileProvider.debugInfoEnabled(true);

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  .state('welcome', {
    url: "/welcome",
    data: {
      permissions: {
        except: ['isloggedin'],
        redirectTo: 'tab.home'
      }
    },
    templateUrl: "templates/welcome.html",
    controller: 'WelcomeCtrl'
  })
  .state('login', {
    url: "/security/login",
    data: {
      permissions: {
        except: ['isloggedin'],
        redirectTo: 'tab.home'
      }
    },
    templateUrl: "templates/login.html",
    controller: 'LoginCtrl as vm'
  })
  .state('forgotpassword', {
    url: "/security/forgot-password",
    data: {
      permissions: {
        except: ['isloggedin'],
        redirectTo: 'tab.home'
      }
    },
    templateUrl: "templates/forgot-password.modal.html"
  })
  .state('register', {
    url: "/register",
    data: {
      permissions: {
        except: ['isloggedin'],
        redirectTo: 'tab.home'
      }
    },
    templateUrl: "templates/register.html",
    controller: 'RegisterCtrl as vm'
  })
  .state('registeruser', {
    url: "/register/user",
    data: {
      permissions: {
        except: ['isloggedin'],
        redirectTo: 'tab.home'
      }
    },
    templateUrl: 'templates/register-user.html',
    controller: 'RegisterUserCtrl as vm'
  })
  .state('registeruserpro', {
    url: "/register/userpro",
    data: {
      permissions: {
        except: ['isloggedin'],
        redirectTo: 'tab.home'
      }
    },
      templateUrl: "templates/register-pro.html",
      controller: 'RegisterProCtrl as vm'
  })
  // setup an abstract state for the tabs directive
  .state('tab', {
    url: "/tab",
    data: {
      permissions: {
        except: ['anonymous'],
        redirectTo: 'welcome'
      }
    },
    abstract: true,
    templateUrl: "templates/tabs.html",
    controller: 'HomeCtrl'
  })
  // Each tab has its own nav history stack:
  .state('tab.home', {
    url: '^/home',
    data: {
      permissions: {
        except: ['anonymous'],
        redirectTo: 'welcome'
      }
    },
    views: {
      'tab-home': {
        templateUrl: 'templates/tab-home.html',
        controller: 'HomeCtrl as vm'
      }
    }
  })
  .state('tab.vehicle-list', {
      url: '^/vehicle/list',
      views: {
        'tab-vehicle': {
          templateUrl: 'templates/tab-vehicle.list.html',
          controller: 'VehicleListCtrl as vm'
        }
      }
  })
  .state('tab.vehicle-detail', {
      url: '^/vehicle/{{id:int}}',
      views: {
        'tab-vehicle': {
          templateUrl: 'templates/tab-vehicle.detail.html',
          controller: 'VehicleEditCtrl as vm'
        }
      }
  })
  .state('tab.vehicle-new', {
      url: '^/vehicle/new',
      views: {
        'tab-vehicle': {
          templateUrl: 'templates/tab-vehicle.detail.html',
          controller: 'VehicleNewCtrl as vm'
        }
      }
  })
    .state('tab.notifications', {
    url: '^/notifications',
    views: {
      'tab-notifications': {
        templateUrl: 'templates/tab-notifications.html',
        controller: 'HomeCtrl as vm',
        parent: "tab"
      }
    }
  })
  ;

  if(!ionic.Platform.isAndroid()) {
  // Optional: For client-side use (Implicit Grant), set responseType to 'token'
    $authProvider.facebook({
      clientId: '1194747453898811',
      responseType: 'token'
    });
  }

 	//$authProvider.loginUrl = 'http://192.168.22.2:8000/api/v1/authenticate';
  $authProvider.loginUrl = 'http://localhost:8000/api/v1/authenticate';

  //$urlRouterProvider.otherwise('/auth');
  // if none of the above states are matched, use this as the fallback
  //$urlRouterProvider.otherwise('/welcome');

  $urlRouterProvider.otherwise(function($injector, $location) {
      var path = '/welcome';
      var as = $injector.get('AuthService');
      as.getAuthUser(function(auth) {
        if (auth.logged_in) {
            path = '/home';
        }
        $location.path(path);
      });
      //return path;
  });
  //$urlRouterProvider.when('/zone','/zone/'+zoneCode);
  //$locationProvider.html5Mode(true);
  $locationProvider.html5Mode({
    enabled: false,
    requireBase: false
  });

}])
.filter('timeAgo', function (){
  var cache = [];
  return function(date) {
    if(typeof cache[date] === 'string')return cache[date];
    var prettyDate = moment(date, 'X').fromNow();
    cache[date] = prettyDate;
    return prettyDate;
  }
})
;
