"use strict";

starter.services.value('version', '0.1')
.factory('VehicleService', ['$http', function($http) {
    return new VehicleRepository($http);
}]);

window.VehicleRepository = function($http) {
  this.$http = $http;

  var findById = function(id) {
    return $http.get('http://localhost:8000/api/v1/vehicle/'+id);
  };

  var findAll = function(page) {
    if(page && page.length > 0) {
      return $http.get(page);
    }
    return $http.get('http://localhost:8000/api/v1/vehicle');
  };

  var addVehicle = function(vehicle) {
    console.log(vehicle);
    return $http.post('http://localhost:8000/api/v1/vehicle', vehicle);
  };

  var updateVehicle = function(vehicle) {
    console.log(vehicle);
    return $http.put('http://localhost:8000/api/v1/vehicle/' + id, vehicle);
  }

  var deleteVehicle = function(id) {
    console.log(id);
    return $http.delete('http://localhost:8000/api/v1/vehicle/' + id);
  }

  return {
    findById: findById,
    findAll: findAll,
    addVehicle: addVehicle,
    updateVehicle: updateVehicle,
    deleteVehicle: deleteVehicle
  };
};

window.VehicleRepositoryMock = function($http) {
  this.$http = $http;
  this.vehicles = $http.get('data/vechicles.json');

  var findById = function(id) {
    return vehicles[id];
  };

  var findAll = function() {
    return $http.get('data/vechicles.json');
  };

  var addVehicle = function(vehicle) {
    console.log(vehicle);
    vehicles.push(vehicle);
  };

  var updateVehicle = function(vehicle) {
    console.log(vehicle);
    vehicles.push(vehicle);
  }

  var deleteVehicle = function(id) {
    console.log(id);
    vehicles.delete(id);
  }
  return {
    findById: findById,
    findAll: findAll,
    addVehicle: addVehicle,
    updateVehicle: updateVehicle
  };
};
