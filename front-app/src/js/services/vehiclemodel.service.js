"use strict";

starter.services.value('version', '0.1')
.factory('VehicleModelService', ['$http', function($http) {
    return new VehicleModelRepository($http);
}]);

window.VehicleModelRepository = function($http) {
  this.$http = $http;

  var findAll = function() {
    return $http.get('http://localhost:8000/api/v1/vehiclemodel');
  };

  return {
    findAll: findAll
  };
};

window.VehicleModelRepositoryMock = function($http) {
  this.$http = $http;
  this.vehicles = $http.get('data/vechiclemodels.json');

  var findAll = function() {
    return this.vehicles;
  };

  return {
    findAll: findAll
  };
};
