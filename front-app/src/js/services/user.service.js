"use strict";

starter.services.value('version', '0.1')
.factory('UserService', ['$http', function($http) {
    return new UserRepository($http);
}]);

window.UserRepository = function($http) {
  // For the purpose of this example I will store user data on ionic local storage but you should save it on a database
  var setUser = function(user_data) {
    window.localStorage.starter_facebook_user = JSON.stringify(user_data);
  };

  var getUser = function() {
    return JSON.parse(window.localStorage.starter_facebook_user || JSON.stringify('{}'));
  };

  var resetPassword = function(user) {
    if(user.email || user.email == '') {
      return false;
    }
    return true;
  }

  var addUser = function() {

     $http.post('http://localhost:8000/api/v1/users', {
         body: vm.user,
         user_id: $rootScope.currentUser.id
     }).success(function(response) {
         // console.log(vm.users);
         // vm.users.push(response.data);
         //vm.users.unshift(response.data);
         //console.log(vm.users);
         //vm.user = '';
         // alert(data.message);
         // alert("User Created Successfully");
     }).error(function(){
       console.log("error");
     });
  };

  var updateUser = function(user){
   console.log(user);
   $http.put('http://localhost:8000/api/v1/users/' + user.id, {
         body: user,
         user_id: user.id
     }).success(function(response) {
         alert("User Updated Successfully");
     }).error(function(){
       console.log("error");
     });
  }

  return {
    getUser: getUser,
    setUser: setUser,
    resetPassword: resetPassword,
    addUser: addUser,
    updateUser: updateUser
  };
};
