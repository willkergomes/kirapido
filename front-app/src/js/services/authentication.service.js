"use strict";

starter.services.value('version', '0.1')
.factory('AuthService', ['$auth', '$http', '$timeout', function($auth, $http, $timeout) {
    return new AuthRepository($auth, $http, $timeout);
}]);

window.AuthRepository = function($auth, $http, $timeout) {
  var vm = this;

  var getAuthUser = function(callback) {
    $http.get('http://localhost:8000/api/v1/authenticate/user')
    .success(function(response) {
      var user = JSON.stringify(response.user);
      localStorage.setItem('user', user);

      vm.logged_in = true;
      vm.loginError = false;
      vm.user = user;
      callback({
        logged_in: vm.logged_in,
        loginError: vm.loginError,
        data: vm.user
      });
    })
    .error(function(error) {
      localStorage.setItem('user', null);
      vm.logged_in = false;
      vm.loginError = true;
      vm.loginErrorText = error.error;
      callback({
        logged_in: vm.logged_in,
        loginError: vm.loginError,
        loginErrorText: vm.loginErrorText
      });
    });
  }

  var login = function(email, password, callback) {

    var credentials = {
      email: email,
      password: password
    };
    $auth.login(credentials).then(function() {
      $timeout(function() {
        getAuthUser(callback)
  		}, 1000);
    })
    .catch(function(error){
      vm.loginError = true;
      localStorage.setItem('user', null);
      if(error.status > 0) {
        localStorage.setItem('user', null);
        vm.loginErrorText = error.data.error;
      } else {
        vm.loginErrorText = 'Não foi possível se conectar ao servidor. Verifique sua conexão e tente novamente.';
      }
      callback({
        logged_in: false,
        loginError: vm.loginError,
        loginErrorText: vm.loginErrorText
      });
    });
  };

  return {
    login: login,
    getAuthUser: getAuthUser
  };
};
