// Karma configuration
// Generated on Fri May 20 2016 13:51:23 GMT-0300 (Hora Padrão de Buenos Aires)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine', 'requirejs'],


    // list of files / patterns to load in the browser
    files: [
      'node_modules/angular/angular.js',
      //'node_modules/angular-*/angular-*.js',
      'node_modules/angular-mocks/angular-mocks.js',
      'test-main.js',
      {pattern: 'src/js/**/*.js', included: true},
      {pattern: 'test/unit/**/*.test.js', included: true}
    ],


    // list of files to exclude
    exclude: [
    ],


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter

    //     Possible Values:
    //     dots
    //     progress
    //     junit
    //     growl
    //     coverage
    reporters: ['progress', 'coverage', 'junit'],

    junitReporter: {
        outputDir: 'reports/junit/',
        outputFile: 'unit.xml',
        suite: 'unit'
    },

    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    // preprocessors defines which files should be evaluated for coverage
    // *preprocessors and reporters are required to generate code coverage report*
    // We're not using the recursive pattern (**) because we don't want the
    // vendor directory and all angularjs app files are flat under javascripts
    preprocessors: {
        'src/js/**/*.js': ['coverage'],
        'test/unit/**/*.test.js': ['coverage']
    },

    // coverageReporter.type
    //     Possible Values:
    //          html (default)
    //          lcov (lcov and html)
    //          lcovonly
    //          text
    //          text-summary
    //          cobertura (xml format supported by Jenkins)
    //     If you set type to text or text-summary, you may set the file option, like this.
    //          coverageReporter : {
    //              type : 'text',
    //              dir : 'coverage/',
    //              file : 'coverage.txt'
    //          }
    coverageReporter: {
        type: 'html',
        dir : 'reports/coverage/'
    },


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['PhantomJS', 'Chrome'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity
  })
}
